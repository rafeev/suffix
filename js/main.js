$( document ).ready(function() {

	$("#header-slider").owlCarousel({
		navigation : false, // Show next and prev buttons
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true
	})

	$("#numbers").owlCarousel({
		navigation : true, // Show next and prev buttons
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		navigationText:	["<",">"]
	})

	$('.show-menu').click(function() {
		$('body').toggleClass('topmenu-show');
	});


	
});